# wTorrent - rTorrent

Wrapper for rTorrent.

*This package is used for wrap generic methods for [wTorrent](https://www.npmjs.com/package/wtorrent)*

If you want complete wrapper for Transmission, use https://www.npmjs.com/package/xmlrpc and follow documentation on https://github.com/rakshasa/rtorrent/wiki/RPC-Setup-XMLRPC

## Usage

```javascript
const wRtorrent = require('wtorrent-rtorrent');

const client = wRtorrent({
  host: '127.0.0.1', // required
  port: 80, // required
  endpoint: '/RPC2', // optional, default /RPC2
  user: 'admin', // optional, default null
  password: 'admin', // optional, default null
});

const torrents = await client.get();
const torrent = await client.getOne('TORRENT_HASH');
```